<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncyclopediaArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('encyclopedia_articles', function (Blueprint $table) {

            $table->id();

            $table->string('title');
            $table->string('slug');
            $table->longText('body')->nullable()->default(null);
            $table->boolean('published');
            $table->unsignedBigInteger('encyclopedia_category_id');

            $table->foreign('encyclopedia_category_id')->references('id')->on('encyclopedia_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encyclopedia_articles');
    }
}
