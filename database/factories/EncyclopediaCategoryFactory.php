<?php

namespace Database\Factories;

use App\Models\EncyclopediaCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EncyclopediaCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EncyclopediaCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->word(10);
        return [
            'name' => $name,
            'slug' => Str::slug($name)
        ];
    }
}
