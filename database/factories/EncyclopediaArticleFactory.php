<?php

namespace Database\Factories;

use App\Models\EncyclopediaArticle;
use App\Models\EncyclopediaCategory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EncyclopediaArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EncyclopediaArticle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->unique()->sentence();
        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'body' => $this->faker->text(2000),
            'published' => $this->faker->boolean(),
            'encyclopedia_category_id' => EncyclopediaCategory::all()->random()->id,
        ];
    }
}
