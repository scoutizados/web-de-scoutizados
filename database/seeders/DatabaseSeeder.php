<?php

namespace Database\Seeders;

use App\Models\EncyclopediaArticle;
use App\Models\EncyclopediaCategory;
use App\Models\Image;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            EncyclopediaCategorySeeder::class
        ]);

        $articles = EncyclopediaArticle::factory(50)->create();
        foreach($articles as $article){
            Image::factory(1)->create([
                'imageable_id' => $article->id,
                'imageable_type' => EncyclopediaArticle::class
            ]);
        }
    }
}
