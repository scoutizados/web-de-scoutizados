<?php

namespace Database\Seeders;

use App\Models\EncyclopediaCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EncyclopediaCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        $name = "Fuegos";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/fuegos.jpg',
            'description' => 'Para calentarnos cuando hace frío, para cocinar o alumbrar, el fuego es una herramienta escencial, pero peligrosa. ¡Dominarlo es indispensable para un scout!'
        ]);

        $name = "Cabuyería";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/cabuyeria.jpg',
            'description' => 'La disciplina de hacer nudos, usada por bomberos, marineros y montañistas, nos trae muchas utilidades que podemos usar en campamento. Además, los amarres nos permiten hacer construcciones para vivir más cómodamente en la naturaleza.'
        ]);

        $name = "Orientación";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/orientacion.jpg',
            'description' => 'Si te quedas sin señal o sin batería, el GPS no te servirá de nada. Aprende las técnicas que usaron los hombres en la antigüedad para guiarse en los océanos y cuando se adentraban en tierras desconocidas. ¡No vuelvas a perderte nunca!'
        ]);

        $name = "Hacha";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/hacha.jpg',
            'description' => 'La leña es el principal combustible que encontraremos para hacer fuego. Tendrás que saber cortarla para hacer buenas fogatas... sin cortarte una pierna en el intento.'
        ]);

        $name = "Carpas";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/carpas.jpg',
            'description' => 'Es bonito dormir mirando las estrellas, hasta que sentimos el frío nocturno, la humedad ¡y ni hablar si empieza a llover! Aprende del armado de carpas para dormir plácidamente en la naturaleza.'
        ]);

        $name = "Mochila";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/mochila.jpg',
            'description' => 'Si no sabes cómo cargarla y utilizarla, una mochila puede ser un verdadero castigo sobre tu espalda. ¡Aprende más sobre ella para sacar al máximo su potencial sin lastimarte!'
        ]);

        $name = "Criptografía";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/criptografia.jpg',
            'description' => 'Usada en las guerras para comunicarse sin que el enemigo sepa de qué se está hablando. Hoy en día, utilizada en informática para preservar la seguridad de la información. ¿Quieres saber qué técnicas existen para cifrar mensajes?'
        ]);

        $name = "Historia";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/historia.jpg',
            'description' => 'Tal vez te interese saber de dónde salió toda esta locura que es el movimiento scout. Aprende sobre sus inicios, el propósito de su existencia y acontecimientos misteriosos e interesantes que rodearon su fundación.'        ]);

        $name = "Ley, principios y virtudes";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/ley-principios-y-virtudes.jpg',
            'description' => 'Los scouts somos personas de palabra. Y prometemos cumplir con nuestra ley. Aprende cuál es la ley scout para saber si estás dispuesto a cumplirla.'
        ]);

        $name = "Construcciones";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/construcciones.jpg',
            'description' => 'La diferencia entre sentirte un habilidoso explorador o un desgraciado en el medio de un bosque puede estar en tu habilidad para elaborar un útil y cómodo rincón de patrulla.'
        ]);

        $name = "Cancionero";
        EncyclopediaCategory::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => '/images/categories/default.jpg',
            'description' => 'Para inspirar a compañeros en una agotadora caminata, para hacer que simples veladas y fogones sean mágicos e inolvidables. ¡Usa este arte para que todos disfruten de un gran momento!'
        ]);
    }
}
