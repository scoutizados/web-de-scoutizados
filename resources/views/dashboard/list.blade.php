@extends('dashboard.layouts.main')

@section('styles')
<link rel="stylesheet" type="text/css" href="/css/confirmation-message.css">
@endsection

@section('icon', $icon)
@section('title', $title)
@section('content')

@if(session()->has('success'))
    <div class="alert alert-success">
        <i class="fas fa-thumbs-up"></i>
        {{ session()->get('success') }}
    </div>
@endif

<div class="confirm hide" id="sign">
    <div class="confirm__window">
        <div class="confirm__titlebar">
            <span class="confirm__title">Borrar artículo</span>
            <button class="confirm__close">&times;</button>
        </div>
        <div class="confirm__content">¿Estás seguro que querés borrar el artículo?</div>
        <div class="confirm__buttons">
            <button class="confirm__button confirm__button--ok confirm__button--fill btn btn-danger"><i class="fas fa-trash"></i>Sí, mandale mecha</button>
            <button class="confirm__button confirm__button--cancel btn btn-success"><i class="fas fa-ban"></i>Uh, no, toqué sin querer</button>
        </div>
    </div>
</div>


<form class="search-form" action="#" method="GET">
    <div class="row">
        <div class="col-left search-box">
            <div class="form-group">
                <input type="text" class="form-control" name="query" placeholder="Buscar palabra clave..." value="{{ request()->input('query') }}">
                <span class="text-danger">@error('query'){{ $message }} @enderror</span>
             </div>
        </div>
        <div class="col-right search-button">
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i>Buscar</button>
            </div>           
        </div>
    </div>
    
 </form>

<table class="table table-striped">
    <tbody>
        @foreach($nodes as $node)
            <tr>
                <td>@if($node->published) <i class="fas fa-eye"></i> @else <i class="fas fa-eye-slash"></i> @endif<a class="title" href="/enciclopedia/articulo/{{$node->slug}}">{{$node->title}}</a></td>
                <td><a class="btn btn-primary" href="/dashboard/edit/article/{{$node->id}}"><i class="fas fa-edit"></i>{{__('Editar')}}</a></td>
                <td><form action="/dashboard/delete/article/{{$node->id}}" method="POST">@csrf<button type="button" class="btn btn-danger btn-delete"><i class="fas fa-trash"></i>{{__('Eliminar')}}</button></form></td>
            </tr>
        @endforeach       
    </tbody>
</table>
<div class="paginator">
    {{--  --}}
    {{$nodes->appends(['query' => $searchText])->links('pagination::bootstrap-4')}}
</div>

<script src="/js/confirmation-message.js"></script>
{{-- @section('scripts')


<script>
    document.querySelector('.btn-delete').addEventListener('click', () => {
      Confirm.open({
        title: 'Background Change',
        message: 'Are you sure you wish the background color?',
        onok: () => {
          document.body.style.backgroundColor = 'blue';
        }
      })
    });
</script>   
@endsection --}}
    
@endsection