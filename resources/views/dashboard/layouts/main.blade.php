<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Panel de control | Scoutizados</title>
        
        <link href="/css/dashboard.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="/packages/fontawesome/css/all.min.css">
        @yield('styles')

    </head>
    <body class="sb-nav-fixed">
        @include('dashboard.layouts.header')

        <div id="layoutSidenav">
            @include('dashboard.layouts.sidebar')



            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4"><i class="@yield('icon')"></i>@yield('title')</h1>
                        @yield('content')                  
                </main>
                @include('dashboard.layouts.footer')
            </div>
        </div>
        <script src="/js/dashboard.js"></script>
        <script src="/packages/bootstrap/js/bootstrap.bundle.min.js"></script>
        @yield('scripts')
    </body>
</html>
