<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid px-4">
        
        <div class="image">
            <img id="oso" src="/images/oso.png" width="100" alt="">
            <div class="text-bubble" id="bubble">
                <p id="dialog"></p>
                <div class="right-point"></div>
            </div>
        </div>
        <div class="name">
            <div class="text-muted">Scoutizados 2021</div>
        </div>

    </div>
</footer>