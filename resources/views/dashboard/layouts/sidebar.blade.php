<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading"><i class="fas fa-book"></i>{{__('Enciclopedia')}}</div>
                <a class="nav-link" href="{{ route('add.article')}}">
                    <i class="fas fa-book-medical"></i>
                    {{__('Agregar artículo')}}
                </a>
                <a class="nav-link" href="{{ route('edit.articles') }}">
                    <i class="fas fa-edit"></i>
                    {{__('Editar artículo')}}
                </a>
                <div class="sb-sidenav-menu-heading"><i class="fas fa-gamepad"></i>{{__('Aplicaciones')}}</div>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-laptop-medical"></i>
                    {{__('Agregar aplicación')}}
                </a>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-edit"></i>
                    {{__('Editar aplicación')}}
                </a>

                <div class="sb-sidenav-menu-heading"><i class="fas fa-store"></i>Tienda</div>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-cart-plus"></i>
                    {{__('Agregar producto')}}
                </a>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-edit"></i>
                    {{__('Editar producto')}}
                </a>

                <div class="sb-sidenav-menu-heading"><i class="fas fa-users"></i>Usuarios</div>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-user-plus"></i>
                    {{__('Agregar usuario')}}
                </a>
                <a class="nav-link" href="index.html">
                    <i class="fas fa-user-edit"></i>
                    {{__('Editar usuario')}}
                </a>

                <div class="sb-sidenav-menu-heading"><i class="fas fa-desktop"></i></i>Web</div>
                <a class="nav-link" href="/">
                    <i class="fas fa-home"></i>
                    {{__('Volver al inicio')}}
                </a>

    
            </div>
        </div>
    </nav>
</div>