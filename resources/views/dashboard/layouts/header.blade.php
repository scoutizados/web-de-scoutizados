<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <!-- Navbar Brand-->
    <a class="navbar-brand ps-3" href="/dashboard"><i class="fas fa-cogs"></i><span class="page-title">{{__('Panel de control')}}</span></a>
    
    <!-- Sidebar Toggle-->
    <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
    
    <!-- Log out -->
    <div class="collapse navbar-collapse justify-content-end">
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4 navbar-nav flex-row flex-wrap bd-navbar-nav pt-2 py-md-0 navbar-right">
            <li class="nav-item dropdown">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-dropdown-link class="nav-link" :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                                        <i class="fas fa-sign-out-alt"></i>
                        {{ __('Salir') }}
                    </x-dropdown-link>

                </form>
            </li>
        </ul>
        
    </div>
</nav>