@extends('dashboard.layouts.main')

@section('title', 'Panel de control')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success">
            <i class="fas fa-thumbs-up"></i>
            {{ session()->get('success') }}
            @if(session()->has('url'))
            <a href="{{ session()->get('url') }}">Ver qué bien te quedó</a>
            @endif
        </div>
    @endif
    <div class="row">

        <div class="col-md-6 col-xl-3">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-book"></i>
                    Enciclopedia
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <a href="{{ route('add.article')}}"><i class="fas fa-book-medical"></i>Agregar artículo</a>
                        </li>
                        <li>
                            <a href="{{ route('edit.articles') }}"><i class="fas fa-edit"></i>Editar artículo</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-gamepad"></i>
                    Juegos y aplicaciones
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <a href=""><i class="fas fa-laptop-medical"></i>Agregar aplicación</a>
                        </li>
                        <li>
                            <a href=""><i class="fas fa-edit"></i>Editar aplicación</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-store"></i>
                    Tienda
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <a href=""><i class="fas fa-cart-plus"></i>Agregar producto</a>
                        </li>
                        <li>
                            <a href=""><i class="fas fa-edit"></i>Editar producto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-users"></i>
                    Usuarios
                </div>
                <div class="card-body">
                    <ul>
                        <li>
                            <a href=""><i class="fas fa-user-plus"></i>Agregar usuario</a>
                        </li>
                        <li>
                            <a href=""><i class="fas fa-user-edit"></i>Editar usuario</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

@endsection