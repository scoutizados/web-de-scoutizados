@extends('dashboard.layouts.main')

@section('icon', $icon)

@section('title', $title)

@section('content')
@if(session()->has('error'))
        <div class="alert alert-success">
            <i class="fas fa-thumbs-up"></i>
            {{ session()->get('error') }}
        </div>
    @endif
    <form action="{{$action}}" method="POST" id="edit" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="title" class="form-label">{{__('Título')}}</label>
            <input class="form-control" id="title" name ="title" type="text" value="{{old('title',$article->title??null)}}">
            @error('title')
            <span class="error-message">
                {{$message}}
            </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="slug" class="form-label">{{__('Alias de URL')}}</label>
                    <input class="form-control" name="slug" id="slug" type="text"  value="{{old('slug',$article->slug??null)}}">
                    @error('slug')
                    <span class="error-message">
                        {{$message}}
                    </span>
                    @enderror
        
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="mb-3">
                    <label for="encyclopedia_category_id" class="form-label">{{__('Categoría')}}</label>
                    <select name="encyclopedia_category_id" id="encyclopedia_category_id" class="form-control">
                        <option value="" @if($article == null) selected @endif disabled>-</option>
                        @foreach ($categories as $category)
                            <option value="{{$category->id}}" @if($article != null && $category->id ==old('encyclopedia_category_id',$article->encyclopedia_category_id)) selected @endif>{{$category->name}}</option>        
        
                        @endforeach
                    </select>
                    @error('encyclopedia_category_id')
                    <span class="error-message">
                        {{$message}}
                    </span>
                    @enderror
                </div>                
            </div>
        </div>

        <div class="row mb-3">
            <label for="file">Imagen que se mostrará en el artículo</label>
            <div class="image-wrapper">
                <img src="@isset($article->image){{$article->image->url}} @else /images/articles/default.jpg @endif" class="img-thumbnail" width="200" id="picture">
            </div>
            <input type="file" class="form-control-file" name="image" id="file" accept="image/png, image/gif, image/jpeg">
            @error('image')
                <span class="error-message">
                    {{$message}}
                </span>
            @enderror
        </div>

        <div class="mb-3">
            <label for="body" class="form-label">{{__('Cuerpo')}}</label>
            <textarea class="form-control" id="body" name="body">
                {{old('body',$article->body??null)}}
            </textarea>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="published" name="published" value="1" @if($article != null && $article->published) checked="checked" @endif>
            <label class="form-check-label" >Publicar contenido</label><br>
        </div>
        <button type="submit" form="edit"  class="btn btn-primary"><i class="fas fa-upload"></i>Guardar</button>
    </form>
@endsection

@section('scripts')
<script src="/js/auto-slug.js"></script>
<script src="/js/image-loader.js"></script>
{{-- <script src="/packages/ckeditor/ckeditor.js"></script>
<script src="/packages/ckeditor/translations/es.js"></script>

<script>
    ClassicEditor
    .create( document.querySelector( '#body' ),{
        language: 'es'
    } )
    .catch( error => {
        console.error( error );
    } );
</script> --}}
<script src="https://cdn.tiny.cloud/1/8nahxskfgcq602pw261fft6d17kj4qkl2eq7qnk9y8ueqqdi/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      language : "es",
      selector: '#body',
      plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak code',
      toolbar_mode: 'floating',
   });
  </script>

@endsection