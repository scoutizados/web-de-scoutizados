@extends('main.layouts.main')

@section('styles')
<link rel="stylesheet" type="text/css" href="/css/search-box.css">
@endsection

@section('content')
<main class="main-node">
    <div class="container">
        <h1>Resultado de búsqueda: "{{$search}}"</h1>
        
        @include('main.articlesList')

    </div>
</main>
@endsection