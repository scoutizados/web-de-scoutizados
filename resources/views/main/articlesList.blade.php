@forelse ($articles as $article)
    
    <div class="article-block">
        <div class="row">
            <div class="col-2 image-container" style="background-image:url(@if($article->image){{$article->image->url}}@else /images/articles/default.jpg @endif)">
            </div>
            <div class="col-8">
                <div class="link-container">
                    <a href="/enciclopedia/articulo/{{$article->slug}}">{{$article->title}}</a>
                </div>
            </div>
        </div>
    </div>                    
@empty
    <div class="not-allowed">
        <img src="/images/sad.png" alt="">          
    </div> 

    <h2 style="text-align: center">¡Ups! Parece que no hay nada por aquí.</h2> 
@endforelse
