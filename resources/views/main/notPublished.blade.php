@guest
    <h2>¡Ups! Lo sentimos, no tenés permiso para ver esta página</h2>
    <div class="not-allowed">
        <i class="fas fa-heart-broken"></i>            
    </div> 
@endguest

@auth
<div class="alert alert-warning" role="alert">
    <i class="fas fa-exclamation-triangle"></i>
    Este contenido no está publicado. ¡Editalo para que los pibes lo puedan ver!
  </div>  
@endauth