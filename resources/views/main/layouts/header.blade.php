<header class="navbar navbar-expand-md navbar-dark bd-navbar fixed-top">
    <nav aria-label="Main navigation" class="container flex-wrap flex-md-nowrap">
        <a class="navbar-brand" href="/"><img src="/images/logo-mini.png" class="logo-mini"><span class="page-title">Scoutizados</span></a>
        <form action="/enciclopedia/buscar" method="GET" class="search-box">
            <input type="search" name="search">
            <i class="fa fa-search"></i>
        </form>


        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#bdNavbar" aria-controls="bdNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" class="bi" fill="currentColor" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"></path>
            </svg>

        </button>

        <div class="collapse navbar-collapse justify-content-end" id="bdNavbar">
            <ul class="navbar-nav flex-row flex-wrap bd-navbar-nav pt-2 py-md-0 navbar-right">
                <li class="nav-item col-6 col-md-auto">
                    <a class="nav-link p-2" href="/">Inicio</a>
                </li>
                <li class="nav-item col-6 col-md-auto">
                    <a class="nav-link p-2" href="/enciclopedia">Enciclopedia</a>
                </li>
                @auth
                <li class="nav-item col-6 col-md-auto">
                    <a class="nav-link p-2" href="/juegos">Juegos</a>
                </li>
                <li class="nav-item col-6 col-md-auto">
                    <a class="nav-link p-2" href="/tienda">Tienda</a>
                </li>
                
                    <li class="nav-item col-6 col-md-auto">
                        <a class="nav-link p-2" href="/dashboard">Panel de control</a>
                    </li>    
                @endauth
            </ul>
        </div>
    </nav>
</header>