<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>@yield('title')</title>

        <link rel="stylesheet" type="text/css" href="/packages/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/packages/fontawesome/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="/css/main.css">
        <link rel="stylesheet" type="text/css" href="/css/search-box-header.css">


        @yield('styles')
    
    </head>
    <body>
        @include("main.layouts.header")
        @yield("content")
        @include("main.layouts.footer")

        <script src="/packages/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/js/header.js"></script>

        @yield('scripts')
    </body>
</html>