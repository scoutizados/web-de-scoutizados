<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Contacto</h2>
                <p><i class="fas fa-envelope"></i><a href="mailto:contacto@scoutizados.com">contacto@scoutizados.com</a></p>
                <p><i class="fas fa-map-marker-alt"></i>En algún lugar de Buenos Aires</p>
            </div>
            <div class="col-md-4">
                <h2>Redes</h2>
                <p class="social-icons">
                    <a href="https://www.youtube.com/channel/UCQ_5pNHpi4JO0FlVudISRBQ" target="_blank"><i class="fab fa-youtube" target="_blank"></i></a>
                    <a href="https://facebook.com/scoutizados" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://instagram.com/scoutizados" target="_blank"><i class="fab fa-instagram"></i></a>
                </p>
                <p>
                </p>
                <p>
                </p>
            </div>
            <div class="col-md-4">
                <h2 class="title" style="font-family:grobold">SCOUTIZADOS<h2>
            </div>
        </div>	
    </div>
</footer>