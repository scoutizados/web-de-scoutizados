@extends('main.layouts.main')

@section('content')
<main class="main-node categories">
    <div class="container">    
        <div class="row">
            @foreach ($categories as $category)
                <div class="col-lg-4">
                    <a class="category-link" href="/enciclopedia/categoria/{{$category->slug}}">
                        <div class="category-image" style="background-image:url({{$category->image}})">
                            <h4 class="category-name">{{$category->name}}</h4>
                        </div>
                    </a>
                </div>  
            @endforeach
        </div>
    </div>
</main>
@endsection

@section('scripts')
<script src="/js/categories.js"></script>
@endsection