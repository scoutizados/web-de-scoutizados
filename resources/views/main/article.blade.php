@extends('main.layouts.main')

@section('title', 'Artículo')

@section('content')
<main class="main-node">
    <div class="container">

        @if(!$article->published)
            @include('main.notPublished')
        @endif

        @if(Auth::user() || $article->published)

            <h3><a href="/enciclopedia"><i class="fas fa-caret-left"></i>Categorías</a> / <a href="/enciclopedia/categoria/{{$category->slug}}">{{$category->name}}</a></h3>
            <h1>{{$article->title}}</h1>
            @auth
            <a href="/dashboard/edit/article/{{$article->id}}">Editar</a>   
            @endauth
            <div class="row">
                <div class="col-xl-8">
                    <div class="body">
                        <div class="article-image" style="background-image:url(@isset($article->image){{$article->image->url}}@else /images/articles/default.jpg @endif)"></div>
                        {!!$article->body!!}
                    </div>
                </div>
                <div class="col-xl-4">
                    <h2>Más sobre {{strtolower($category->name)}}</h2>
                    @include('main.articlesList')
                </div>
            </div>

        @endif
    </div>
</main>
@endsection