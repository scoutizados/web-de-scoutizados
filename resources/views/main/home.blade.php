@extends('main.layouts.main')

@section('title', 'Scoutizados')

@section('content')
    <main>
        <div class="welcome-section">
            <img src="/images/logo-big.png" class="logo-big">
            <h1 class="slogan">{{__('Por un mundo mejor')}}</h1>
        </div>

        <div class="links-section">
            <div class="container">
                <h1 class="section-title section">{{__('Enlaces')}}</h1>
                <div class="row">
                    <div class="col-md-4">
                        <div class="icon-container">
                            <i class="fas fa-book"></i>						
                        </div>
                        <h3>{{__('Enciclopedia')}}</h3>
                        <p>Aprende cosas nuevas en nuestra gran biblioteca virtual.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-container">
                            <i class="fas fa-gamepad"></i>
                        </div>
                        <h3>{{__('Juegos y aplicaciones')}}</h3>
                        <p>Diviértete y aprende con nuestros juegos.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-container">
                            <i class="fas fa-store"></i>
                        </div>
                        <h3>{{__('Tienda')}}</h3>
                        <p>Explora nuestros interesantes artículos a la venta.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mission-section section">
            <div class="container">
                <h1 class="section-title">{{__('Misión')}}</h1>
                <div class="row">
                    <div class="col-md-6">
                        <p>El objetivo de <b>scoutizados</b> es utilizar la poderosa herramienta que es internet para brindarle todo el conocimiento necesario a todos los scouts del mundo. De esta manera creemos que ayudaremos a cumplir el objetivo del movimiento mientras disfrutamos de la aventura que es el proceso.</p>
                    </div>
                    <div class="col-md-6">
                        <i class="fas fa-globe-americas"></i>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
