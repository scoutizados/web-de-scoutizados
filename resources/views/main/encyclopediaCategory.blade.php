@extends('main.layouts.main')
@section('content')
<main class="main-node">
    <div class="container">
        <h3><a href="/enciclopedia"><i class="fas fa-caret-left"></i>Categorías</a></h3>
        <h1>{{$category->name}}</h1>

        <div class="description">
            {{$category->description}}
        </div>

        @include('main.articlesList')

    </div>
</main>
@endsection