<?php

namespace App\Http\Controllers;

use App\Models\EncyclopediaArticle;
use App\Models\EncyclopediaCategory;
use Illuminate\Support\Facades\File; 
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class EncyclopediaController extends Controller
{
    public function editArticles(Request $request)
    {
        $searchText = $request->input('query');

        $nodes = EncyclopediaArticle::where('title','LIKE','%'.$searchText.'%')
            ->latest('id')
            ->paginate(10);
            
        $title = "Artículos de la enciclopedia";
        $icon = "fas fa-book";

        return view('dashboard.list', compact('nodes', 'title', 'icon', 'searchText'));
    }


    public function showArticle(Request $request)
    {
        $article = EncyclopediaArticle::where('slug', $request->slug)->first();

        if(!$article) return abort(404);

        $category = EncyclopediaCategory::where('id', $article->encyclopedia_category_id)->first();

        $articles = EncyclopediaArticle::where('encyclopedia_category_id', $article->encyclopedia_category_id)
            ->where('published', true)
            ->where('id', '!=', $article->id)
            ->latest('id')
            ->take(6)
            ->get();

        return view('main.article', compact('article', 'category', 'articles'));
    }


    public function showCategories()
    {
        $categories = DB::select(
            'SELECT name, encyclopedia_category_id, image, encyclopedia_categories.slug 
            FROM encyclopedia_articles 
            INNER JOIN encyclopedia_categories ON encyclopedia_articles.encyclopedia_category_id = encyclopedia_categories.id
            WHERE published = true
            GROUP BY name, encyclopedia_category_id, image, encyclopedia_categories.slug'
        );

        return view('main.encyclopedia', compact('categories'));
    }


    public function showArticlesByCategory(Request $request)
    {
        $categorySlug = $request->category;
        $category = EncyclopediaCategory::where('slug', $categorySlug)->first();

        $articles = EncyclopediaArticle::where([
            ['encyclopedia_category_id', $category->id],
            ['published', true]
            ])->get();


        return view('main.encyclopediaCategory', compact('articles', 'category'));
    }


    public function editArticle(Request $request)
    {
        $article = EncyclopediaArticle::where('id', $request->id)->first();

        if(!$article) return abort(404);

        $categories = EncyclopediaCategory::all();
        $title = "Editar artículo";
        $icon = "fas fa-edit";
        $action = "/dashboard/edit/article/" . $article->id;

        return view('dashboard.editArticle', compact('article', 'title', 'icon', 'categories', 'action'));
    }


    public function addArticle()
    {
        $article = null;
        $categories = EncyclopediaCategory::all();

        $title = "Agregar artículo";
        $icon = "fas fa-book-medical";

        $action = "/dashboard/add/article";

        return view('dashboard.editArticle', compact('article', 'categories', 'title', 'icon', 'action'));
    }


    public function storeArticle(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|unique:encyclopedia_articles|max:255',
            'slug'  => 'required|unique:encyclopedia_articles|max:255',
            'encyclopedia_category_id' => 'required',
            'image' => 'image',
        ]);   
        
        $article = new EncyclopediaArticle();

        $this->setArticleData($article, $request);

        if($request->image)
        {
            $url = $this->uploadImage($request);
            $article->image()->create([
                'url' => $url
            ]);
        }

        return redirect('/dashboard')
            ->with('success', 'Se creó el artículo con éxito. ¡Seguí así y en unos meses somos wikipedia!')
            ->with('url', '/enciclopedia/articulo/' . $article->slug);
    }
    

    public function updateArticle(Request $request)
    {
        $article = EncyclopediaArticle::where('id', $request->id)->first();
        
        $validated = $request->validate([
            'title' => 'required|max:255',
            'slug'  => 'required|unique:encyclopedia_articles,slug,'. $article->id .'|max:255',
            'encyclopedia_category_id' => 'required',
            'image' => 'image',
        ]);
        
        $this->setArticleData($article, $request);

        if($request->image)
        {
            $url = $this->uploadImage($request);
            if($article->image)
            {
                File::delete(public_path() . $article->image->url);
                $article->image->update([
                    'url' => $url,
                ]);
            }
            else
            {
                $article->image()->create([
                    'url' => $url
                ]);    
            }
        }

        return redirect('/dashboard')
            ->with('success', 'Se editó el artículo correctamente. ¡Bien ahí, capo!')
            ->with('url', '/enciclopedia/articulo/' . $article->slug);
    }


    private function uploadImage(Request $request)
    {
        $image = $request->file('image');

        $name = 'img_'.Str::random(15) .'.'. $image->extension();
        $location = '/images/articles/';
        $diskLocation = public_path(). $location;
        $image->move($diskLocation, $name);

        return $location . $name;
    }


    private function setArticleData(EncyclopediaArticle $article, Request $request)
    {
        $article->title = $request->title;
        $article->slug = $request->slug;
        $article->encyclopedia_category_id = $request->encyclopedia_category_id;
        $article->body = $request->body;

        $published = ($request->published != null);
        $article->published = $published;

        $article->save();  
    }


    public function searchArticle(Request $request)
    {
        $search = $request->search;

        $articles = EncyclopediaArticle::where('title', 'LIKE', "%{$search}%")->get();
        
        return view('main.articleSearcher', compact('search', 'articles'));
    }


    public function deleteArticle(Request $request)
    {
        $article = EncyclopediaArticle::where('id',$request->id)->first();
        if($article->image)
        {
            File::delete(public_path() . $article->image->url);
            $article->image->delete();
        }
        $article->delete();

        return redirect('/dashboard/articles')->with('success', 'Se eliminó el artículo correctamente. Adiós artículo, que la fuerza te acompañe.');
    }
}
