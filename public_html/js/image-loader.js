document.getElementById("file").addEventListener('change', function(event){

    
    let file = event.target.files[0];
    let reader = new FileReader();
    
    reader.onload = (event) => {
        console.log(event);
      document.getElementById("picture").setAttribute("src", event.target.result);  
    };

    reader.readAsDataURL(file);
});