/*!
    * Start Bootstrap - SB Admin v7.0.1 (https://startbootstrap.com/template/sb-admin)
    * Copyright 2013-2021 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
    */
    // 
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

    let dialogs = [
        'Tengo hambre.',
        'No me gusta trabajar.',
        'Soy sexy y barrigón.',
        '¡Trabajá, chimi!',
        'Está para unos mates.',
        'Sí, rascame la pancita.',
        '¿Qué querés?',
        'No me toques.',
        'Mis pasiones: river, comer y dormir.',
        'Qué lindo día para no laburar.',
        '¡Acá estoy! ¡Tiembla la parrilla!',
        '¿Gordo yo? No, tus ojos tienen mucho aumento.',
        'Debería perder peso, pero yo odio perder.',
        'Tengo el abodomen marcadísimo... por el resorte del calzón.',
        'Estoy a dos kilos de que greenpeace me proteja.',
        'No estoy gordo, estoy esponjoso.',
        'No soy flojo, nací cansado.',
        'De los 3 kilos que quería bajar, me faltan 11.',
        '¡Cuando estoy triste nunca bajo la cabeza! Se me ve la papada.',
        '¿Los araganes vamos al cielo o nos vienen a buscar?',
        '¡Hoy me levanté con ganas de trabajar! Y me acosté para que se me pasara.',
        'El trabajo es sagrado, no lo toques.',
        'Si el trabajo es salud, que trabajen los enfermos.'
    ];

    let bubbleActive = false;

    document.getElementById('oso').addEventListener('click', function (e) {
        if( bubbleActive) return;
        bubbleActive = true;
        var bubble = document.getElementById('bubble');
        var dialog = document.getElementById('dialog');
        var bear = document.getElementById('oso');

        var rnd = Math.floor(Math.random() * dialogs.length);
        dialog.innerHTML = dialogs[rnd];
        bear.classList.add("active");
        bubble.classList.add("active");


        setTimeout(function(){ 
            bubble.classList.add("out");
            setTimeout(function(){
                bubble.classList.remove("active"); 
                bubble.classList.remove("out");
                bear.classList.remove("active");

                bubbleActive = false;
            },400);
        }, 4000);
      });

});
