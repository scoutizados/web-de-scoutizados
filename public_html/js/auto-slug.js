let title = document.getElementById("title");
let slug = document.getElementById("slug");

title.addEventListener("change", function(event){
    let value;
    
    value = title.value.toLowerCase();
    value = value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    value = value.replace(/ /g, "-");

    slug.value = value;
});