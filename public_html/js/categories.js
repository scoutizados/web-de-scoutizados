
let links = document.getElementsByClassName("category-link");


for(let i = 0; i < links.length; i++)
{
    links[i].addEventListener("mouseover", function(event){
        let name = event.currentTarget.getElementsByClassName("category-name")[0];
        name.classList.add("over");
    });

    links[i].addEventListener("mouseout", function(event){
        let name = event.currentTarget.getElementsByClassName("category-name")[0];
        name.classList.remove("over");
    });
}