<html>
    <head>
        <link rel="stylesheet" href="logo-3d.css">
    </head>
    <body>
    <div class="textbox">
        <div class="textbox-box">
            <div class="textbox-face textbox-side"></div>
            <div class="textbox-face textbox-bottom"></div>
            <div class="textbox-face textbox-top"></div>
            <div class="textbox-field">
                <div class="textbox-label">Encontranos en:</div>
                <input class="textbox-text" type="text" placeholder="Escribir aquí..." />
            </div>
            <div class="textbox-action">
                <div class="textbox-face textbox-side"></div>
                <div class="textbox-face textbox-top"></div>
                <div class="textbox-face textbox-bottom"></div>
                <svg viewBox="0 0 24 24">
                  <path d="M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z"></path>
                </svg>
            </div>
        </div>
    </div>
</body>
</html>