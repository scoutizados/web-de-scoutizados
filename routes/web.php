<?php

use App\Http\Controllers\EncyclopediaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('main.home');});

require __DIR__.'/auth.php';

Route::get('/enciclopedia/articulo/{slug}', [EncyclopediaController::class, 'showArticle'])->name('show.article');

Route::get('/enciclopedia', [EncyclopediaController::class, 'showCategories'])->name('show.categories');

Route::get('/enciclopedia/categoria/{category}', [EncyclopediaController::class, 'showArticlesByCategory'])->name('show.articles.by.category');

Route::get('/enciclopedia/buscar', [EncyclopediaController::class, 'searchArticle'])->name('search.article');

Route::get('/dashboard', function () {return view('dashboard.home');})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard/articles', [EncyclopediaController::class, 'editArticles'])->middleware(['auth'])->name('edit.articles');

Route::get('/dashboard/edit/article/{id}', [EncyclopediaController::class, 'editArticle'])->middleware(['auth'])->name('edit.article');

Route::post('/dashboard/edit/article/{id}', [EncyclopediaController::class, 'updateArticle'])->middleware(['auth'])->name('update.article');

Route::get('/dashboard/add/article', [EncyclopediaController::class, 'addArticle'])->middleware(['auth'])->name('add.article');

Route::post('/dashboard/add/article', [EncyclopediaController::class, 'storeArticle'])->middleware(['auth'])->name('store.article');

Route::post('/dashboard/delete/article/{id}', [EncyclopediaController::class, 'deleteArticle'])->middleware(['auth'])->name('delete.article');
